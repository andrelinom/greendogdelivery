package br.com.agsouza.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.agsouza.domain.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {

}
