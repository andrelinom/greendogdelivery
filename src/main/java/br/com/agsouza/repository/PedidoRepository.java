package br.com.agsouza.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.agsouza.domain.Pedido;

public interface PedidoRepository extends JpaRepository<Pedido, Long> {

}
