package br.com.agsouza;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;



//@EnableEurekaClient
@ComponentScan(basePackages = "br.com.agsouza")
@SpringBootApplication
public class GreendogdeliveryNewApplication {

	public static void main(String[] args) {
		SpringApplication.run(GreendogdeliveryNewApplication.class, args);
	}
}
